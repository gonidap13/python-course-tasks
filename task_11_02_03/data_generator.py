# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_11_02_03.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


def generate_file(filename="data.txt", clients=50):
    """Генерирует список клиентов в заданном формате.

    Генерирует список из clients клиентов в заданном формате и
    записывает в файл filename.

    Аргументы:
      - filename: имя создаваемого файла;
      - clients: кол-во клиентов (10-100).

    Возвращает:
      - имя созданного файла.
    """
    import random
    import datetime

    assert 10 <= clients <= 100

    data = {
        "m": {
            "f": ["Петров", "Николаев", "Семенов", "Крутов", "Федоров",
                  "Кашин", "Маликов", "Лягушкин"],
            "i": ["Петр", "Игорь", "Семен", "Юрий", "Михаил", "Олег",
                  "Павел", "Александр"],
            "o": ["Петрович", "Игоревич", "Семенович", "Юрьевич",
                  "Михайлович", "Олегович", "Павлович", "Александрович"]
        },
        "f": {
            "f": [],
            "i": ["Анна", "Екатерина", "Анастасия", "Елена", "Наталья",
                  "Ольга", "Мария", "Ксения"],
            "o": ["Петровна", "Игоревна", "Семеновна", "Юрьевна",
                  "Михайловна", "Олеговна", "Павловна", "Александровна"]
        }
    }
    data["f"]["f"] = [x + "а" for x in data["m"]["f"]]
    date_sep = [".", "/", "-"]
    sep = [" ", "\t"]
    polis_type = {"Транспорт": 50000,
                  "Недвижимость": 100000,
                  "Путешествия": 20000,
                  "Здоровье": 15000}

    lines = []
    for i in range(clients):
        key = random.choice(["m", "f"])
        # имя
        full_name = "{sep1}{f}{sep2}{i}{sep3}{o}{sep4}".\
                    format(sep1=random.choice(sep)*random.randint(1, 3),
                           sep2=random.choice(sep)*random.randint(1, 3),
                           sep3=random.choice(sep)*random.randint(1, 3),
                           sep4=random.choice(sep),
                           f=random.choice(data[key]["f"]),
                           i=random.choice(data[key]["i"]),
                           o=random.choice(data[key]["o"])
                           )

        reg_prob = random.random()
        if reg_prob <= 0.2:
            full_name = full_name.lower()
        elif reg_prob <= 0.4:
            full_name = full_name.upper()

        # Дата
        b_sep = random.choice(date_sep)
        prec_d = random.choice(["%d"])
        prec_m = random.choice(["%m"])
        prec_y = random.choice(["%y", "%Y"])
        dt = datetime.datetime(random.randint(1950, 1980),
                               random.randint(1, 12),
                               random.randint(1, 28))
        ff = "{prec_d}{b_sep}{prec_m}{b_sep}{prec_y}{sep1}".format(
                                b_sep=b_sep,
                                prec_d=prec_d,
                                prec_m=prec_m,
                                prec_y=prec_y,
                                sep1=random.choice(sep)*random.randint(1, 3))
        birthday = dt.strftime(ff)
        if random.random() <= 0.5:
            birthday = birthday.replace(b_sep + "0", b_sep)

        # Тип полиса и сумма
        polis_key = random.choice([key for key in polis_type.keys()])
        polis = polis_key + random.choice(sep)

        # Сумма
        money = str(polis_type[polis_key])

        line = "".join([full_name, birthday, polis, money])
        # print(line)

        if i + 1 < clients:
            line += "\n"
        lines.append(line)

    with open(filename, "w", encoding="utf-8") as f:
        f.writelines(lines)
    return filename


if __name__ == "__main__":
    generate_file()
