# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_06_02_03.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


def foo(s):
    """!!!

    Параметры:
        - s (str): строка.

    Сложность: !!!.
    """
    val = 0
    for c in s:
        if c.isdigit():
            val += int(c)
    return val
