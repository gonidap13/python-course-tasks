# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_13_02_01.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


import sys
import os.path
import re
import datetime
import time
import smtplib
from email.mime.text import MIMEText
import requests


NO_DATA = "Нет данных"


class CurrencyChecker:
    """Класс CurrencyChecker реализует валютный помощник.

    Поля:
        Данные ИТ-аккаунта (прописаны в коде)
        - self.it_email: e-mail ИТ-отдела;
        - self.it_email_password: пароль от e-mail ИТ-отдела;
        - self.it_email_smtp_server: SMTP-сервер для e-mail ИТ-отдела;
        - self.it_email_smtp_port: SMTP-порт для e-mail сервера ИТ-отдела;
        Данные руководителя
        - self.ceo_name: ФИО руководителя;
        - self.ceo_email: e-mail руководителя.
        Прочее
        - self.log_filename: имя файла для логгирования;
        - self.msg: письмо для отправки (email.mime.text).

    Методы:
      - self._log(): вывод события на экран и в лог-файл;
      - self.run(): бесконечный цикл работы - получение данных и их отправка;
      - self.get_info(): получение котировок с сайта;
      - self._create_message(): формирование текста сообщения для отправки.
    """

    def __init__(self, ceo_name, ceo_email):
        """Конструктор класса."""
        # Данные руководителя
        raise NotImplementedError
        # Уберите raise и дополните код

        # Данные ИТ-аккаунта
        raise NotImplementedError
        # Уберите raise и дополните код

        # Лог-файл
        # Имя лог-файла задано в коде - "log.txt" в папке приложения
        raise NotImplementedError
        # Уберите raise и дополните код

    def __str__(self):
        """Вернуть информацию о классе."""
        return "CurrencyChecker v 0.1"

    def _log(self, message):
        """Вывести на экран 'message' с указанием текущего времени
        формате 0001-01-01 00:00:00 (год, месяц, число, часы, минуты, секунды):
          - на экран;
          - в лог-файл (дописать в конец файла).
        """
        raise NotImplementedError
        # Уберите raise и дополните код

    def run(self, timeout, currencies):
        """Каждые 'timeout секунд':
        - запросить информацию о курсах;
        - отправить их на почту.
        """
        while True:
            try:
                info = !!!
                self._log("Данные получены: " + str(sorted(info.items())))
                text = !!!
                raise NotImplementedError
                # Уберите raise и дополните код

                self._log("Письмо успешно отправлено!")
            except Exception as err:
                self._log("Произошла ошибка: " + str(err))
            finally:
                time.sleep()

    @staticmethod
    def get_info(currencies):
        """Вернуть курсы валют.

        Источник: http://www.finanz.ru/valyuty/v-realnom-vremeni-rub.

        Параметры:
            - currencies: список валют, например: ['USD', 'EUR'].

        Результат:
            - словарь вида {
                              'USD': (20.0, "10:23:00"),
                              'EUR': (30.0, "10:23:00")
                            }
                , где значение (кортеж) содержит:
                  - значение покупки (3-й столбец) (float);
                  - дату получения значения с биржи (последний столбец) (str).

              Если валюта из 'currencies' или какое-либо значение не найдено,
              валюта добавляется в словарь со значением "Не известно".
        """

        def _value_to_float(value):
            """Вернуть 'value' как вещественное число (если возможно)
            или вернуть 'NO_DATA'."""
            try:
                return float(value.replace(',', '.'))
            except ValueError:
                return NO_DATA

        # 1. По умолчанию о каждой валюте ничего не известно
        raise NotImplementedError
        # Уберите raise и дополните код
        res = !!!

        # 2. Поиск информации о валютах
        # . не захватывает переносы строки по умолчанию, поэтому при создании
        # regex-объекта необходимо указать флаг re.DOTALL
        raise NotImplementedError
        # Уберите raise и дополните код
        reg_ex = r"!!!"


        raise NotImplementedError
        # Уберите raise и дополните код

    def _create_message(self, info):
        """Составить и вернуть текст письма.

        Валюты выводятся в алфавитном порядке.

        Параметры:
          - info: словарь из self.get_info().

        Результат:
          - текст письма (str) вида:

            Иван Иванович!

            Обновленные курсы валют:
              - EUR: 60.65 (12:00:05)
              - USD: 50.12 (12:00:05)

            С уважением, ИТ-отдел.
        """

        res = """\
{ceo_name}!

Обновленные курсы валют:
{info_str}

С уважением, ИТ-отдел.\
"""

        raise NotImplementedError
        # Уберите raise и дополните код

    def send_mail(self, text):
        """Отправить текст 'text' на почтовый адрес
        'self.ceo_email' для 'self.ceo_name'.
        """

        # 1. Формирование сообщения (MIMEText)
        self.msg = MIMEText(text)
        raise NotImplementedError
        # Уберите raise и дополните код

        # 2. Подклюение к серверу и отправка письма
        raise NotImplementedError
        # Уберите raise и дополните код
